nova_network_and_inject_key:
  cmd:
    - script
    - source: salt://essex/nova_sec.sh
    - template: jinja
    - require:
      - file: /tmp/id_rsa.pub
      - pkg: nova
      - cmd: run_sync_nova
      - service: nova-services
